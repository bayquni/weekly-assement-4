# How the workflow for git until it's updated to repository ?

Make sure you have to commit with 'git commit -m description' and then, push to the branch with 'git push origin'

# Whats git? What's git use for?

Git has the functionality, performance, security and flexibility that most teams and individual developers need. These attributes of Git are detailed above. In side-by-side comparisons with most other alternatives, many teams find that Git is very favorable


# How to track git progress ?

If progress tracking is enabled, a progress bar appears above the project board, on the project listing page, in the issue sidebar, and in references to the project on other project boards.

